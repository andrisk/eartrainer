# Ear Trainer

Music ear trainer

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/eartrainer.andrisk)

## Building
Ear Trainer is built using [clickable](https://clickable.bhdouglass.com/en/latest/).

Before building the application, piano sound files must be provided. First option is to generate the sound files using two scripts in samples folder.

1. run samples\generate_midi.py. The python3 script requires mido python library. It can be installed using `pip3 install mido` command.

2. run samples\generate_wav.sh shell script. The script requires timidity, ffmpeg and wavegain. The script generates wave files, trims them to the 1 second length and normalizes volume of the wave files. The script will also generate resource file for qt that ensures that all the generated sound files are packaged with the application at build.

Note that you need to set a soundfont for timidity++ before generating the sound files. Soundfonts such as [freepats-general-midi](https://freepats.zenvoid.org/SoundSets/general-midi.html) can usually be installed from a repository of linux distribution. Path to the installed soundfont must be set in a timidity.cfg (usually in /etc/timidity folder). For more in depth reference on configuring timidity++ software synthesizer see [archwiki](https://wiki.archlinux.org/index.php/Timidity%2B%2B).

## Roadmap

version 0.4:
 - [x] chord trainer

version 0.5+:
 - [ ] user training statistics 
 - [ ] training by singing intervals 
 - [ ] gamify the learning process - mini game controlled by pitch captured by microphone (sometimes motivation needs some help)
 - [ ] possibility to select different instruments
 - [ ] maybe link the app with synthesizer so it would not be needed to include sounds in the package
 
have some cool ideas for new features want to help improve the application? Your pull request is very welcomed. (As long as no AI like ChatGPT or GitHub copilot was used because of uncertainty about license rights of the code it produces.)

## License

Copyright (C) 2020 - 2024  Andrej Trnkóci

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

[![GPLv3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)](http://www.gnu.org/licenses/)
