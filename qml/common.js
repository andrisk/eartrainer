/*
 * Copyright (C) 2020 - 2022  Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * FretboardTrainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//.pragma library

function semitonesToName(id)
{
    return [i18n.tr("Unison"), i18n.tr("Minor Second"), i18n.tr("Major Second"), i18n.tr("Minor Third"), i18n.tr("Major Third"),
            i18n.tr("Perfect Fourth"), i18n.tr("Tritone"), i18n.tr("Perfect Fifth"), i18n.tr("Minor Sixth"), i18n.tr("Major Sixth"),
            i18n.tr("Minor Seventh"), i18n.tr("Major Seventh"), i18n.tr("Perfect Octave")][id]
}

function semitonesToShortName(id)
{
    return [i18n.tr("PP"), i18n.tr("m2"), i18n.tr("M2"), i18n.tr("m3"), i18n.tr("M3"), i18n.tr("P4"), i18n.tr("TT"), i18n.tr("P5"),
            i18n.tr("m6"), i18n.tr("M6"), i18n.tr("m7"), i18n.tr("M7"), i18n.tr("P8")][id]
}

function getMinimum(first, second)
{
    return Math.min(first, second)
}

function getMaximum(first,second)
{
    return Math.max(first, second)
}

function getCeiling(num)
{
    return Math.ceil(num)
}

function getNumberOfEnabledValuesInList(list)
{
    var result = 0
    for (var x = 0; x < list.length; x++)
    {
        if (list[x].isEnabled)
        {
            result ++
        }
    }
    return result
}
