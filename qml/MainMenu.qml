/*
 * Copyright (C) 2022  Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * eartrainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "common.js" as Common


Page
{
    signal intervalTrainerButtonClicked()
    signal chordTrainerButtonClicked()

    GridLayout
    {
        id: mainMenuGrid
        rows: 1
        columns: 2
        anchors.fill:parent
        property int buttonSize: Common.getMinimum(parent.width / 2,  parent.height) * 0.9

        Button
        {
            text: i18n.tr("Interval trainer")
            Layout.preferredWidth: mainMenuGrid.buttonSize
            Layout.preferredHeight: mainMenuGrid.buttonSize
            Layout.rightMargin: (mainMenuGrid.width - mainMenuGrid.buttonSize * 2) / 5
            Layout.leftMargin: (mainMenuGrid.width - mainMenuGrid.buttonSize * 2) / 5
            Layout.topMargin: (mainMenuGrid.height - mainMenuGrid.buttonSize ) / 5
            Layout.bottomMargin: (mainMenuGrid.height - mainMenuGrid.buttonSize ) / 5
            onClicked: intervalTrainerButtonClicked()
        }
        Button
        {
            text: i18n.tr("Chord trainer")
            Layout.preferredWidth: mainMenuGrid.buttonSize
            Layout.preferredHeight: mainMenuGrid.buttonSize
            Layout.rightMargin: (mainMenuGrid.width - mainMenuGrid.buttonSize * 2) / 5
            Layout.leftMargin: (mainMenuGrid.width - mainMenuGrid.buttonSize * 2) / 5
            Layout.topMargin: (mainMenuGrid.height - mainMenuGrid.buttonSize ) / 5
            Layout.bottomMargin: (mainMenuGrid.height - mainMenuGrid.buttonSize ) / 5
            onClicked: chordTrainerButtonClicked()
        }
    }
}
