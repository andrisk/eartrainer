/*
 * Copyright (C) 2020 - 2022  Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * FretboardTrainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import "common.js" as Common


Grid
{
    id: intervalKeyboardRoot

    property list<ListeningTrainerOption> questionOptions

    property int numberOfButtons: questionOptions.length
    columns: getColumnCount()
    property int rows: Common.getCeiling(numberOfButtons / columns)

    rowSpacing: 0
    columnSpacing: 0

    property int buttonWidth: width / columns
    property int buttonHeight: height / rows

    property string opacityAnimationColor: "red"

    OpacityAnimator
    {
        id: buttonOpacityAnimator
        from: 1.0
        to: 0.0
        duration: 1000
        running: true
    }

    Repeater
    {
        id: intervalKeyboardRepeater
        model: numberOfButtons

        Button
        {
            enabled: intervalKeyboardRoot.questionOptions[index].isEnabled //intervalKeybardRoot.intervalEnabledStateList[index]
            width: buttonWidth
            height: buttonHeight
            hoverEnabled: false
            contentItem: Text
            {
                text: intervalKeyboardRoot.questionOptions[index].shortText;
                font.pixelSize: height / 2
                color: theme.palette.normal.baseText
                minimumPointSize: 8
                fontSizeMode: Text.Fit
                verticalAlignment: Qt.AlignVCenter
                horizontalAlignment: Qt.AlignHCenter
            }

            Rectangle
            {
                anchors.fill: parent
                color: opacityAnimationColor
                opacity: 0.0
            }

            onClicked:
            {
                answer(index)
            }
        }
    }

    function getColumnCount()
    {
        for (let cols = 1; cols < numberOfButtons; cols++) //lets consider we use "cols" columns
        {
            var requiredRows = Math.ceil(numberOfButtons / cols); //we can calculate how many rows we need at least to position all buttons
            var buttonWidth = width / cols;
            var buttonHeight = height / requiredRows;
            if ((buttonWidth / buttonHeight > 0.5) && (buttonWidth / buttonHeight < 2)) //if we get buttons with reasonable shape we accept the solution
            {
                return Math.ceil(numberOfButtons / requiredRows); //but if we can fit buttons to less columns, we do it even if we get not so square buttons
            }
        }
        return width > height ? numberOfButtons : 1;
    }

    function runButtonOpacityAnimation(keyId, color)
    {
        buttonOpacityAnimator.target = intervalKeyboardRepeater.itemAt(keyId).children[0]
        opacityAnimationColor = color;
        buttonOpacityAnimator.restart()
    }
}
