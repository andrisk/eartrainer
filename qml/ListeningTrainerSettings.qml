/*
 * Copyright (C) 2022 Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * FretboardTrainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import "common.js" as Common
import NotePlayer 1.0

Page
{
    id: chordTrainerSettingsPage
    property alias lowestAllowedNoteId: intervalRangeSelector.lowestAllowedNoteId
    property alias highestAllowedNoteId: intervalRangeSelector.highestAllowedNoteId

    property bool isPitchRangeValid: intervalRangeSelector.highestAllowedNoteId - intervalRangeSelector.lowestAllowedNoteId > 12
    property bool isQuestionOptionsSelectionValid: Common.getNumberOfEnabledValuesInList(questionOptions) > 1
    property bool isNotesPlayStylesSelectionValid: Common.getNumberOfEnabledValuesInList(notesPlayStyles) > 0

    property string invalidPitchRangeMessage: i18n.tr("Pitch range must be more than one octave") + "."
    property string invalidQuestionOptionsSettingsMessage
    property string invalidNotesPlayStylesSettingsMessage

    property bool areSettingsValid: isPitchRangeValid && isQuestionOptionsSelectionValid && isNotesPlayStylesSelectionValid
    property string invalidSettingsMessage: getInvalidSettingsMessage()

    signal trainingSessionButtonClicked()

    property string questionOptionsTitle
    property string notesPlayStylesTitle

    property list<ListeningTrainerOption> questionOptions
    property list <ListeningTrainerPlayStyle> notesPlayStyles
    property int contentMargin: 20

    Flickable
    {
        anchors.fill: parent
        clip: true
        contentWidth: mainColumn.width
        contentHeight: mainColumn.height
        flickableDirection: Flickable.VerticalFlick

        leftMargin: contentMargin
        rightMargin: contentMargin
        bottomMargin: contentMargin
        topMargin: contentMargin
        ColumnLayout
        {
            id: mainColumn

            Label
            {
                Layout.fillWidth: true
                Layout.preferredWidth: chordTrainerSettingsPage.width - contentMargin * 2
                Layout.topMargin: 10
                Layout.bottomMargin: 10
                text: i18n.tr("Pitch range") + ":"
                font.bold: true
                color: isPitchRangeValid ? theme.palette.normal.baseText : "red"
            }


            IntervalRangeSlider
            {
                id: intervalRangeSelector
                Layout.preferredWidth: chordTrainerSettingsPage.width - contentMargin * 2
                Layout.fillWidth: true
            }

            Label
            {
                Layout.fillWidth: true
                Layout.preferredWidth: chordTrainerSettingsPage.width - contentMargin * 2
                Layout.topMargin: 20
                Layout.bottomMargin: 10
                text: notesPlayStylesTitle
                font.bold: true
                color: isNotesPlayStylesSelectionValid ? theme.palette.normal.baseText : "red"
            }

            Repeater
            {
                Layout.fillWidth: true
                Layout.preferredWidth: chordTrainerSettingsPage.width - contentMargin * 2
                model: chordTrainerSettingsPage.notesPlayStyles
                delegate: SwitchDelegate {
                    text: modelData.text
                    Layout.preferredWidth: chordTrainerSettingsPage.width - contentMargin * 2
                    Layout.fillWidth: true
                    hoverEnabled: false
                    checked: modelData.isEnabled
                    z: 1
                    onToggled:
                    {
                        modelData.isEnabled = checked
                    }
                }
            }

            Label
            {
                Layout.fillWidth: true
                Layout.preferredWidth: chordTrainerSettingsPage.width - contentMargin * 2
                Layout.topMargin: 20
                Layout.bottomMargin: 10
                text: questionOptionsTitle
                font.bold: true
                color: isQuestionOptionsSelectionValid ? theme.palette.normal.baseText : "red"
            }

            Repeater
            {
                id: questionOptionsList
                Layout.fillWidth: true
                Layout.preferredWidth: chordTrainerSettingsPage.width - contentMargin * 2
                model: chordTrainerSettingsPage.questionOptions
                delegate: SwitchDelegate {
                    text: modelData.longText
                    Layout.preferredWidth: chordTrainerSettingsPage.width - contentMargin * 2
                    Layout.fillWidth: true
                    hoverEnabled: false
                    checked: modelData.isEnabled
                    z: 1
                    onToggled:
                    {
                        modelData.isEnabled = checked
                    }
                }
            }

            Button
            {
                text: i18n.tr("Start training session")
                hoverEnabled: false
                onClicked: trainingSessionButtonClicked()
                Layout.fillWidth: true
                Layout.preferredWidth: chordTrainerSettingsPage.width - contentMargin * 2
            }

        }
    }

    function settingsValid()
    {
        return intervalRangeSelector.highestAllowedNoteId - intervalRangeSelector.lowestAllowedNoteId > 12 && Common.getNumberOfEnabledValuesInList(questionOptions) > 1 && Common.getNumberOfEnabledValuesInList(notesPlayStyles) > 0
    }

    function getInvalidSettingsMessage()
    {
        if (!isPitchRangeValid)
        {
            return invalidPitchRangeMessage
        }
        else if (!isQuestionOptionsSelectionValid)
        {
            return invalidQuestionOptionsSettingsMessage
        }
        else if (!isNotesPlayStylesSelectionValid)
        {
            return invalidNotesPlayStylesSettingsMessage
        }
        else
        {
            return ""
        }
    }
}
