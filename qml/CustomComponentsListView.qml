/*
 * Copyright (C) 2020 - 2021, 2023 Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * FretboardTrainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import "common.js" as Common
import Lomiri.Components 1.3 as UITK

ListView
{
    id: root
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.verticalCenter: parent.verticalCenter
    width: parent.width - getMarginSpaceForItemSize(parent.width, parent.height)
    height: parent.height - getMarginSpaceForItemSize(parent.width, parent.height)
    spacing: 2

    Component
    {
        id: intervalTypeEnableControl
        SwitchDelegate
        {
            text: modelContent
            hoverEnabled: false

            onCheckedChanged:
            {
                var tempList = intervalEnabledTypeStateList
                tempList[modelItemIndex] = checked
                intervalEnabledTypeStateList = tempList
            }

            Component.onCompleted:
            {
                checked = intervalEnabledTypeStateList[modelItemIndex]
            }
        }
    }

    Component
    {
        id: intervalEnableControl
        SwitchDelegate
        {
            text: modelContent
            hoverEnabled: false
            onCheckedChanged:
            {
                var tempList = intervalEnabledStateList
                tempList[modelItemIndex] = checked
                intervalEnabledStateList = tempList
            }

            Component.onCompleted:
            {
                checked = intervalEnabledStateList[modelItemIndex]
            }
        }
    }

    Component
    {
        id: rangeSliderComponent
        Column
        {
            Text
            {
                id: rangeText
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                text: pianoKeyIndexToNoteName(rangeSelector.first.value) + " - " + pianoKeyIndexToNoteName(rangeSelector.second.value)
                color: highestAllowedNoteId - lowestAllowedNoteId > 12 ? theme.palette.normal.baseText : "red"
            }
            RangeSlider
            {
                id: rangeSelector
                width: parent.width
                stepSize: 1.0
                snapMode: RangeSlider.SnapAlways
                from: 0
                to: 87
                hoverEnabled: false

                first.onValueChanged:
                {
                    lowestAllowedNoteId = first.value
                }

                second.onValueChanged:
                {
                    highestAllowedNoteId = second.value
                }

                Component.onCompleted:
                {
                    //apparently second.value needs to be set first because second.value needs to be always bigger than first.value
                    second.value = highestAllowedNoteId
                    first.value = lowestAllowedNoteId
                }
            }
        }
    }

    Component
    {
        id: categorySeparator
        Label
        {
            text: modelContent
            font.bold: true
            color: settingsCategoryValid[modelItemIndex] ? theme.palette.normal.baseText : "red"
            topPadding: 5
        }
    }

    Component
    {
        id: acceptButton
        Button
        {
            text: modelContent
            hoverEnabled: false
            onClicked: trainingSessionButtonClicked()
        }
    }

    Component
    {
        id: basicAppInfo
        Column
        {
            width: root.width
            UITK.LomiriShape {
                width: Common.getMaximum(root.width, root.height) / 5
                height: width
                radius: "large"
                anchors.horizontalCenter: parent.horizontalCenter
                source: Image {
                    mipmap: true
                    source: "qrc:/logo.svg"
                }
            }

            Label
            {
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr('Ear Trainer')
                //width: parent.width
                height: Common.getMaximum(root.width, root.height) / 15

                font.pixelSize: height * 0.8
            }
            Label
            {
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr("Version") + " " + Qt.application.version
                height: Common.getMaximum(root.width, root.height) / 30
                font.pixelSize: height * 0.8
            }
        }
    }

    Component
    {
        id: linkReference
        Rectangle
        {
            width: parent.width
            height: Common.getMaximum(root.width, root.height) / 20
            color: theme.palette.normal.foreground //"lightgrey"
            radius: height / 3
            border.color: theme.palette.normal.foregroundText //"black"
            border.width: 1
            Label
            {
                text: splitCsvString(modelContent)[0]
                anchors.left: parent.left
                anchors.right: nextSymbol.left
                height: parent.height
                padding: parent.height / 5
                fontSizeMode: Text.Fit
                font.pixelSize: height
                MouseArea
                {
                    anchors.fill: parent
                    onClicked: Qt.openUrlExternally(splitCsvString(modelContent)[1])
                }
            }
            UITK.Icon
            {
                id: nextSymbol
                name: "next"
                width: parent.height * 0.8
                height: parent.height * 0.8
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                color: theme.palette.normal.foregroundText
                MouseArea
                {
                    anchors.fill: parent
                    onClicked: Qt.openUrlExternally(splitCsvString(modelContent)[1])
                }
            }

            function splitCsvString(str)
            {
                return str.split(",")
            }
        }
    }

    Component
    {
        id: appInfoItem
        Rectangle
        {
            width: parent.width
            height: Common.getMaximum(root.width, root.height) / 20
            color: "lightgrey"
            radius: height / 3
            border.color: "black"
            border.width: 1
            Text
            {
                text: modelContent
                anchors.left: parent.left
                anchors.right: parent.right
                height: parent.height
                padding: parent.height / 5
                fontSizeMode: Text.Fit
                font.pixelSize: height
                MouseArea
                {
                    anchors.fill: parent
                    onClicked: Qt.openUrlExternally(splitCsvString(modelContent)[1])
                }
            }
        }
    }


    delegate: Component
    {
        Loader
        {
            width: root.width
            property string modelContent: modelData.content
            property int modelItemIndex: modelData.index
            sourceComponent:
            {
                switch(modelData.type)
                {
                    case "intervalTypeEnableControl":
                        return intervalTypeEnableControl
                    case "intervalEnableControl":
                        return intervalEnableControl
                    case "rangeSliderComponent":
                        return rangeSliderComponent
                    case "categorySeparator":
                        return categorySeparator
                    case "acceptButton":
                        return acceptButton
                    case "basicAppInfo":
                        return basicAppInfo
                    case "appInfoItem":
                        return appInfoItem
                    case "linkReference":
                        return linkReference
                }
            }
        }
    }

    function getMarginSpaceForItemSize(width, height)
    {
        return Math.min(width, height) /10
    }

    function pianoKeyIndexToNoteName(zeroBasedKeyIndex)
    {
        var octaveNumber = Math.floor((zeroBasedKeyIndex + 9) / 12)
        var zeroBasedNoteIndex = Math.floor((zeroBasedKeyIndex + 9) % 12)
        return noteNameFromCromaticIndex(zeroBasedNoteIndex) + octaveNumber.toString();
    }

    function noteNameFromCromaticIndex(zeroBasedNoteIndex)
    {
        return ["C", "C#", "D","D#","E","F","F#","G","G#","A","A#","B"][zeroBasedNoteIndex % 12]
    }
}



