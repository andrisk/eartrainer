/*
 * Copyright (C) 2020 - 2022 Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * FretboardTrainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import QtQuick 2.0
import QtQuick.Controls 2.2

Column
{
    id: rangeSliderComponent

    property int lowestAllowedNoteId: 0
    property int highestAllowedNoteId: 87
    property bool isRangeHigherThanOctave: highestAllowedNoteId - lowestAllowedNoteId > 12

    Text
    {
        id: rangeText
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        text: pianoKeyIndexToNoteName(rangeSelector.first.value) + " - " + pianoKeyIndexToNoteName(rangeSelector.second.value)
        color: isRangeHigherThanOctave ? theme.palette.normal.baseText : "red"
    }
    RangeSlider
    {
        id: rangeSelector
        width: parent.width
        stepSize: 1.0
        snapMode: RangeSlider.SnapAlways
        from: 0
        to: 87
        hoverEnabled: false

        first.onValueChanged:
        {
            lowestAllowedNoteId = first.value
        }

        second.onValueChanged:
        {
            highestAllowedNoteId = second.value
        }

        Component.onCompleted:
        {
            //apparently second.value needs to be set first because second.value needs to be always bigger than first.value
            second.value = highestAllowedNoteId
            first.value = lowestAllowedNoteId
        }
    }

    function pianoKeyIndexToNoteName(zeroBasedKeyIndex)
    {
        var octaveNumber = Math.floor((zeroBasedKeyIndex + 9) / 12)
        var zeroBasedNoteIndex = Math.floor((zeroBasedKeyIndex + 9) % 12)
        return noteNameFromCromaticIndex(zeroBasedNoteIndex) + octaveNumber.toString();
    }

    function noteNameFromCromaticIndex(zeroBasedNoteIndex)
    {
        return ["C", "C#", "D","D#","E","F","F#","G","G#","A","A#","B"][zeroBasedNoteIndex % 12]
    }
}



