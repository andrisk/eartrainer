/*
 * Copyright (C) 2020 - 2022  Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * eartrainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import QtQuick 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import NotePlayer 1.0
import "common.js" as Common

Page
{
    id: rootPage
    property bool canAnswer: false
    property int firstNote
    //TBD problem - notesCount is not used in IntervalTrainerSettings so selectable range can be different
    property int notesCount: NotePlayer.getNotesCount()

    property int playState: 0
    property int lastIndex: 1

    property int lowestAllowedNoteId: 0
    property int highestAllowedNoteId: 87


    property list<ListeningTrainerOption> questionOptions
    property int currentQuestionOptionIndex
    property list<ListeningTrainerPlayStyle> notesPlayStyles
    property ListeningTrainerPlayStyle currentPlayStyle

    Component
    {
        id: questionOptionComponent
        QtObject
        {
            property int shortText
            property int answerIndex
            property int intervalWidth
            property int expectedPlaybackTimeMs
        }
    }

    signal playSample(variant notesOption, variant notesPlayStyle, int firstNote)

    Timer {
        id: newQuestionCountdown
        interval: 3000;
        running: false;
        repeat: false;
        onTriggered: newQuestion();
    }

    Timer
    {
        id: allowAnswerTimer
        interval: 2200;
        running: false;
        repeat: false;
        onTriggered: questionSamplePlayingFinished();
    }

    Column
    {
        id: mainColumn
        anchors.fill: parent
        Image
        {
            id: resultImage
            height: parent.width < parent.height ? parent.height * (0.35) : parent.height * (0.4)
            width: parent.width
            fillMode: Image.PreserveAspectFit
            source: "qrc:/question.svg"
        }
        Label
        {
            id: correctAnswerText
            height: (parent.height) * 0.1
            width: parent.width
            horizontalAlignment: Qt.AlignHCenter
            text: ""
        }

        Grid
        {
            id: repeatButtonAndSuccesRateBarGrid
            columns: mainColumn.width < mainColumn.height ? 1 : 2
            height: mainColumn.width < mainColumn.height ? (parent.height) * (0.15) : (parent.height) * (0.1)
            width: parent.width
            SuccessRateBar
            {
                id: successRate
                width: mainColumn.width < mainColumn.height ? parent.width : parent.width / 2
                height: mainColumn.width < mainColumn.height ? parent.height / 2 : parent.height
            }
            Button
            {
                id: repeatButton
                text: i18n.tr("Play again")
                width: mainColumn.width < mainColumn.height ? parent.width : parent.width / 2
                height: mainColumn.width < mainColumn.height ? parent.height / 2 : parent.height
                font.pixelSize: height * 0.4
                hoverEnabled: false

                onClicked:
                {
                    playCurrentQuestionSample()
                }
            }
        }


        AnswerKeyboard
        {
            id: keyboard
            questionOptions: rootPage.questionOptions
            height: (parent.height) * (0.4)
            width: parent.width
        }
    }

    function newQuestion()
    {
        resultImage.source = "qrc:/question.svg"
        correctAnswerText.text = ""
        selectRandomQuestionOption()
        playCurrentQuestionSample()
    }

    function selectRandomQuestionOption()
    {
        canAnswer = false;
        var currentQuestionOption = getRandomEnabledItemFromList(questionOptions)
        currentQuestionOptionIndex = currentQuestionOption.answerIndex
        currentPlayStyle = getRandomEnabledItemFromList(notesPlayStyles)
        firstNote = getRandomInRange(lowestAllowedNoteId, highestAllowedNoteId - questionOptions[currentQuestionOptionIndex].intervalWidth)
    }

    function answer(answerIndex)
    {
        if (canAnswer)
        {
            repeatButton.enabled = false
            canAnswer = false

            if (currentQuestionOptionIndex === answerIndex)
            {
                resultImage.source = "qrc:/correct.svg"
                successRate.numberOfCorrect++
                keyboard.runButtonOpacityAnimation(currentQuestionOptionIndex, "green")
            }
            else
            {
                resultImage.source = "qrc:/wrong.svg"
                successRate.numberOfWrong++
                keyboard.runButtonOpacityAnimation(questionOptions[currentQuestionOptionIndex].answerIndex, "red")
            }
            correctAnswerText.text = questionOptions[currentQuestionOptionIndex].longText
            newQuestionCountdown.start()

        }
    }

    function playCurrentQuestionSample()
    {
        canAnswer = false
        repeatButton.enabled = false
        playSample(questionOptions[currentQuestionOptionIndex].notesOption, currentPlayStyle.playStyle, firstNote)
        allowAnswerTimer.interval = currentPlayStyle.expectedPlaybackTimeMs
        allowAnswerTimer.start()
    }

    function getRandomListIndex(list)
    {
        return Math.floor(Math.random() * list.length)
    }

    function getRandomFromList(list)
    {
        return list[Math.floor(Math.random() * list.length)]
    }

    function getRandomEnabledItemFromList(list)
    {
        var enabledList = getFilteredListWithEnabledOnly(list)
        return getRandomFromList(enabledList)
    }

    function getRandomInRange(min, max)
    {
        var optionsCount = max - min + 1
        return Math.floor(Math.random() * (optionsCount)) + min
    }

    function questionSamplePlayingFinished()
    {
        canAnswer = true
        repeatButton.enabled = true
    }

    function resetSuccessRate()
    {
        successRate.numberOfCorrect = 0
        successRate.numberOfWrong = 0
    }

    function getFilteredListWithEnabledOnly(list)
    {
        var resultList = []
        for (var x = 0; x < list.length; x++)
        {
            if (list[x].isEnabled === true)
            {
                resultList.push(list[x])
            }
        }
        return resultList
    }


    function getEnabledIndexListFromBoolList(enabledFlagsList)
    {
        var resultList = []
        for (var x = 0; x < enabledFlagsList.length; x++)
        {
            if (enabledFlagsList[x] === true)
            {
                resultList.push(x)
            }
        }
        return resultList
    }
}
