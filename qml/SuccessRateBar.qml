/*
 * Copyright (C) 2021  Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * eartrainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7

Rectangle
{
    property int numberOfCorrect: 0
    property int numberOfWrong: 0
    property string greenColor: "#FF99FF99"
    property string redColor: "#FFFF9999"
    property string grayColor: "#FFAAAAAA"

    Rectangle
    {
        id: correctRectangle
        color: numberOfCorrect == 0 ? grayColor : greenColor
        width: numberOfCorrect + numberOfWrong == 0 ? parent.width / 2 : (parent.width * numberOfCorrect) / (numberOfCorrect + numberOfWrong)
        z: 0
        anchors
        {
            left: parent.left
            top: parent.top
            bottom: parent.bottom
        }
    }
    Text
    {
        anchors.leftMargin: height / 3
        text: numberOfCorrect
        anchors.fill: parent
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: parent.height * 0.4
        z: 1
    }
    Rectangle
    {
        color: numberOfCorrect + numberOfWrong == 0 ? grayColor : redColor
        z: 0
        anchors
        {
            left: correctRectangle.right
            right: parent.right
            top: parent.top
            bottom: parent.bottom
        }
    }
    Text
    {
        anchors.rightMargin: height / 3
        text: numberOfWrong
        anchors.fill: parent
        horizontalAlignment: Text.AlignRight
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: parent.height * 0.4
        z: 1
    }
}
