/*
 * Copyright (C) 2020 - 2024  Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * eartrainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3 as UITK
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0
import QtMultimedia 5.4
import "common.js" as Common
import NotePlayer 1.0
import Qt.labs.settings 1.0

UITK.MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'eartrainer.andrisk'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Settings
    {
        id: settingsObject
        property alias intervalTrainerLowestAllowedNoteId: basicIntervalTrainerSettings.lowestAllowedNoteId
        property alias intervalTrainerHighestAllowedNoteId: basicIntervalTrainerSettings.highestAllowedNoteId
        property bool intervalTrainerMinSecondEnabled: true
        property bool intervalTrainerMajSecondEnabled: true
        property bool intervalTrainerMinThirdEnabled: true
        property bool intervalTrainerMajThirdEnabled: true
        property bool intervarTrainerPerfectFourthEnabled: true
        property bool intervalTrainerTritoneEnabled: true
        property bool intervalTrainerPerfectFifthEnabled: true
        property bool intervalTrainerMinSixthEnabled: true
        property bool intervalTrainerMajSixthEnabled: true
        property bool intervalTrainerMinSeventhEnabled: true
        property bool intervalTrainerMajSeventhEnabled: true
        property bool intervalTrainerPerfectOctaveEnabled: true
        property bool intervalTrainerAscendingEnabled: true
        property bool intervalTrainerDescendingEnabled: true
        property bool intervalTrainerHarmonicEnabled: true
        property alias chordTrainerLowestAllowedNoteId: chordTrainerSettingsPage.lowestAllowedNoteId
        property alias chordTrainerHighestAllowedNoteId: chordTrainerSettingsPage.highestAllowedNoteId
        property bool chordTrainerMajorEnabled: true
        property bool chordTrainerMinorEnabled: true
        property bool chordTrainerDiminishedEnabled: true
        property bool chordTrainerAugmentedEnabled: true
        property bool chordTrainerHarmonicEnabled: true
        property bool chordTrainerAscendingArpeggionEnabled: true
        property bool chordTrainerDescendingArpeggioEnabled: true
    }

    property list<ListeningTrainerOption> chordTrainerQuestionOptions:
    [
        ListeningTrainerOption { shortText: i18n.tr("maj"); longText: i18n.tr("Major"); notesOption: NotePlayer.ChordQualities.Major; answerIndex: 0; intervalWidth: 7; property alias isEnabled: settingsObject.chordTrainerMajorEnabled },
        ListeningTrainerOption { shortText: i18n.tr("min"); longText: i18n.tr("Minor"); notesOption: NotePlayer.ChordQualities.Minor; answerIndex: 1; intervalWidth: 7; property alias isEnabled: settingsObject.chordTrainerMinorEnabled },
        ListeningTrainerOption { shortText: i18n.tr("dim"); longText: i18n.tr("Diminished"); notesOption: NotePlayer.ChordQualities.Diminished; answerIndex: 2; intervalWidth: 6; property alias isEnabled: settingsObject.chordTrainerDiminishedEnabled },
        ListeningTrainerOption { shortText: i18n.tr("aug"); longText: i18n.tr("Augmented"); notesOption: NotePlayer.ChordQualities.Augmented; answerIndex: 3; intervalWidth: 8; property alias isEnabled: settingsObject.chordTrainerAugmentedEnabled }
    ]

    property list <ListeningTrainerPlayStyle> chordTrainerNotesPlayStyles:
    [
        ListeningTrainerPlayStyle { text: i18n.tr("Harmonic"); playStyle: NotePlayer.ChordPlayStyle.Harmonic; expectedPlaybackTimeMs: 1200; property alias isEnabled: settingsObject.chordTrainerHarmonicEnabled },
        ListeningTrainerPlayStyle { text: i18n.tr("Ascending arpeggio"); playStyle: NotePlayer.ChordPlayStyle.AscendingArpeggio; expectedPlaybackTimeMs: 3200; property alias isEnabled: settingsObject.chordTrainerAscendingArpeggionEnabled },
        ListeningTrainerPlayStyle { text: i18n.tr("Descending arpeggio"); playStyle: NotePlayer.ChordPlayStyle.DescendingArpeggio; expectedPlaybackTimeMs: 3200; property alias isEnabled: settingsObject.chordTrainerDescendingArpeggioEnabled }
    ]

    property list<ListeningTrainerOption> intervalTrainerQuestionOptions:
    [
        ListeningTrainerOption { shortText: Common.semitonesToShortName(1);  longText: i18n.tr("Minor Second"); notesOption: 1; answerIndex: 0; intervalWidth: 1; property alias isEnabled: settingsObject.intervalTrainerMinSecondEnabled},
        ListeningTrainerOption { shortText: Common.semitonesToShortName(2); longText: i18n.tr("Major Second"); notesOption: 2; answerIndex: 1; intervalWidth: 2; property alias isEnabled: settingsObject.intervalTrainerMajSecondEnabled },
        ListeningTrainerOption { shortText: Common.semitonesToShortName(3); longText: i18n.tr("Minor Third"); notesOption: 3; answerIndex: 2; intervalWidth: 3; property alias isEnabled: settingsObject.intervalTrainerMinThirdEnabled  },
        ListeningTrainerOption { shortText: Common.semitonesToShortName(4); longText: i18n.tr("Major Third"); notesOption: 4; answerIndex: 3; intervalWidth: 4; property alias isEnabled: settingsObject.intervalTrainerMajThirdEnabled  },
        ListeningTrainerOption { shortText: Common.semitonesToShortName(5); longText: i18n.tr("Perfect Fourth"); notesOption: 5; answerIndex: 4; intervalWidth: 5; property alias isEnabled: settingsObject.intervarTrainerPerfectFourthEnabled  },
        ListeningTrainerOption { shortText: Common.semitonesToShortName(6); longText: i18n.tr("Tritone"); notesOption: 6; answerIndex: 5; intervalWidth: 6; property alias isEnabled: settingsObject.intervalTrainerTritoneEnabled  },
        ListeningTrainerOption { shortText: Common.semitonesToShortName(7); longText: i18n.tr("Perfect Fifth"); notesOption: 7; answerIndex: 6; intervalWidth: 7; property alias isEnabled: settingsObject.intervalTrainerPerfectFifthEnabled  },
        ListeningTrainerOption { shortText: Common.semitonesToShortName(8); longText: i18n.tr("Minor Sixth"); notesOption: 8; answerIndex: 7; intervalWidth: 8; property alias isEnabled: settingsObject.intervalTrainerMinSixthEnabled  },
        ListeningTrainerOption { shortText: Common.semitonesToShortName(9); longText: i18n.tr("Major Sixth"); notesOption: 9; answerIndex: 8; intervalWidth: 9; property alias isEnabled: settingsObject.intervalTrainerMajSixthEnabled  },
        ListeningTrainerOption { shortText: Common.semitonesToShortName(10); longText: i18n.tr("Minor Seventh"); notesOption: 10; answerIndex: 9; intervalWidth: 10; property alias isEnabled: settingsObject.intervalTrainerMinSeventhEnabled  },
        ListeningTrainerOption { shortText: Common.semitonesToShortName(11); longText: i18n.tr("Major Seventh"); notesOption: 11; answerIndex: 10; intervalWidth: 11; property alias isEnabled: settingsObject.intervalTrainerMajSeventhEnabled  },
        ListeningTrainerOption { shortText: Common.semitonesToShortName(12); longText: i18n.tr("Perfect Octave"); notesOption: 12; answerIndex: 11; intervalWidth: 12; property alias isEnabled: settingsObject.intervalTrainerPerfectOctaveEnabled  }
    ]

    property list <ListeningTrainerPlayStyle> intervalTrainerNotesPlayStyles:
    [
        ListeningTrainerPlayStyle { text: i18n.tr("Ascending intervals"); playStyle: NotePlayer.IntervalTypes.Ascending; expectedPlaybackTimeMs: 2200; property alias isEnabled: settingsObject.intervalTrainerAscendingEnabled },
        ListeningTrainerPlayStyle { text: i18n.tr("Descending intervals"); playStyle: NotePlayer.IntervalTypes.Descending; expectedPlaybackTimeMs: 2200; property alias isEnabled: settingsObject.intervalTrainerDescendingEnabled  },
        ListeningTrainerPlayStyle { text: i18n.tr("Harmonic intervals"); playStyle: NotePlayer.IntervalTypes.Harmonic;  expectedPlaybackTimeMs: 1200; property alias isEnabled: settingsObject.intervalTrainerHarmonicEnabled  }
    ]

    StackView
    {
        id: stack
        initialItem: theMainMenu
        anchors.fill: parent

        MainMenu
        {
            id: theMainMenu
            title: i18n.tr("Main Menu")

            header: UITK.PageHeader
            {
                title: i18n.tr("Main Menu")
                trailingActionBar.actions: [
                    UITK.Action {
                        iconName: "info"
                        text: i18n.tr("Info")
                        onTriggered: stack.push(appInfoPage)
                    }
                ]
            }

            onIntervalTrainerButtonClicked: stack.push(basicIntervalTrainerSettings)
            onChordTrainerButtonClicked: stack.push(chordTrainerSettingsPage)
        }

        ListeningTrainerSettings
        {
            id: chordTrainerSettingsPage
            visible: false
            header: UITK.PageHeader
            {
                title: i18n.tr("Settings")
                trailingActionBar.actions: [
                    UITK.Action {
                        iconName: "tick"
                        text: i18n.tr("OK")
                        onTriggered: stack.acceptChordTrainerSettings()
                    },
                    UITK.Action
                    {
                        iconName: "close"
                        text: i18n.tr("Back")
                        onTriggered: stack.pop()
                    }
                ]
            }

            questionOptions: root.chordTrainerQuestionOptions
            notesPlayStyles: root.chordTrainerNotesPlayStyles
            invalidNotesPlayStylesSettingsMessage: i18n.tr("At least one chord playing style must be enabled") + "."
            invalidQuestionOptionsSettingsMessage:  i18n.tr("At least two chord qualities must be enabled") + "."
            questionOptionsTitle: i18n.tr("Chord qualities") + ":"
            notesPlayStylesTitle: i18n.tr("Chord playing styles") + ":"
        }

        ListeningTrainer
        {
            id: chordTrainerPage
            visible: false
            header: UITK.PageHeader
            {
                title: i18n.tr("Chord trainer")
                trailingActionBar.actions: [
                    UITK.Action
                    {
                        iconName: "close"
                        text: i18n.tr("Back")
                        onTriggered: stack.pop()
                    }
                ]
            }
            questionOptions: root.chordTrainerQuestionOptions
            notesPlayStyles: root.chordTrainerNotesPlayStyles
            onPlaySample: NotePlayer.playChord(firstNote, notesOption, notesPlayStyle)
        }

        ListeningTrainerSettings
        {
            id: basicIntervalTrainerSettings
            visible: false
            header: UITK.PageHeader
            {
                title: i18n.tr("Settings")
                trailingActionBar.actions: [
                    UITK.Action {
                        iconName: "tick"
                        text: i18n.tr("OK")
                        onTriggered: stack.acceptIntervalTrainerSettings()

                    },
                    UITK.Action
                    {
                        iconName: "close"
                        text: i18n.tr("Back")
                        onTriggered: stack.pop()
                    }
                ]
            }

            questionOptions: root.intervalTrainerQuestionOptions
            notesPlayStyles: root.intervalTrainerNotesPlayStyles
            invalidNotesPlayStylesSettingsMessage: i18n.tr("At least one interval type must be enabled") + "."
            invalidQuestionOptionsSettingsMessage: i18n.tr("At least two intervals must be enabled") + "."
            questionOptionsTitle: i18n.tr("Enabled intervals") + ":"
            notesPlayStylesTitle: i18n.tr("Interval types") + ":"
        }

        ListeningTrainer
        {
            id: basicIntervalTrainer

            visible: false

            header: UITK.PageHeader
            {
                title: i18n.tr("Interval trainer")
                trailingActionBar.actions: [
                    UITK.Action {
                        iconName: "close"
                        text: i18n.tr("Close")
                        onTriggered:
                        {
                            stack.pop()
                        }

                    }
                ]
            }

            questionOptions: root.intervalTrainerQuestionOptions
            notesPlayStyles: root.intervalTrainerNotesPlayStyles
            onPlaySample: NotePlayer.playInterval(firstNote, notesOption, notesPlayStyle)
        }

        AppInfo
        {
            id: appInfoPage
            visible: false

            header: UITK.PageHeader
            {
                title: i18n.tr("About")
                trailingActionBar.actions: [
                    UITK.Action {
                        iconName: "close"
                        text: i18n.tr("Close")
                        onTriggered:
                        {
                            stack.pop()
                        }
                    }
                ]
            }
        }

        Component.onCompleted:
        {
            basicIntervalTrainerSettings.trainingSessionButtonClicked.connect(acceptIntervalTrainerSettings)
            chordTrainerSettingsPage.trainingSessionButtonClicked.connect(acceptChordTrainerSettings)
        }

        Dialog
        {
            id: invalidSettingsDialog
            visible: false
            title: i18n.tr("Invalid settings")

            x: (parent.width - width) / 2
            y: (parent.height - height) / 2

            property string textMessage: "";
            standardButtons: Dialog.Ok

            Text
            {
                text: invalidSettingsDialog.textMessage
                color: theme.palette.normal.foregroundText
                anchors.centerIn: parent
            }
        }

        function acceptIntervalTrainerSettings()
        {
            if (!basicIntervalTrainerSettings.areSettingsValid)
            {
                invalidSettingsDialog.textMessage = basicIntervalTrainerSettings.invalidSettingsMessage
                invalidSettingsDialog.open()
                return
            }
            basicIntervalTrainer.lowestAllowedNoteId = basicIntervalTrainerSettings.lowestAllowedNoteId
            basicIntervalTrainer.highestAllowedNoteId = basicIntervalTrainerSettings.highestAllowedNoteId
            basicIntervalTrainer.resetSuccessRate()
            stack.push(basicIntervalTrainer)
            basicIntervalTrainer.newQuestion()
        }

        function acceptChordTrainerSettings()
        {
            if (!chordTrainerSettingsPage.areSettingsValid)
            {
                invalidSettingsDialog.textMessage = chordTrainerSettingsPage.invalidSettingsMessage
                invalidSettingsDialog.open()
                return
            }
            chordTrainerPage.resetSuccessRate()
            chordTrainerPage.lowestAllowedNoteId = chordTrainerSettingsPage.lowestAllowedNoteId
            chordTrainerPage.highestAllowedNoteId = chordTrainerSettingsPage.highestAllowedNoteId
            stack.push(chordTrainerPage)
            chordTrainerPage.newQuestion()
        }
    }
}




