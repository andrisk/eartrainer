#!/bin/bash
if [ -d ./wav/ ] ; then
	rm -r ./wav/
fi
mkdir ./wav/
rm ./wav/samples.qrc
echo "<RCC><qresource prefix=\"/\">" >> ./wav/samples.qrc
for number in {1..88}
do
timidity ./mid/${number}.mid -Ow -o ./wav/${number}_long.wav
ffmpeg -y -i ./wav/${number}_long.wav -t 1.1 ./wav/${number}.wav
wavegain -y ./wav/${number}.wav
rm ./wav/${number}_long.wav
echo "<file>${number}.wav</file>" >> ./wav/samples.qrc
done
echo "</qresource></RCC>"  >> ./wav/samples.qrc
exit 0
