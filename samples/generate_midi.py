from mido import Message, MidiFile, MidiTrack
import os
import shutil

if os.path.isdir("./mid/"):
	shutil.rmtree("./mid/")

os.mkdir("./mid/")

for keyNumber in range(1, 89):
	mid = MidiFile()
	track = MidiTrack()
	mid.tracks.append(track)
	track.append(Message('program_change', program=0, time=0))
	track.append(Message('note_on', note=keyNumber + 20, velocity=127, time=20))
	track.append(Message('note_off', note=keyNumber + 20, velocity=127, time=1080))
	mid.save('./mid/' +  str(keyNumber) + '.mid')
