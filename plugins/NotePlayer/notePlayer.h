/*
 * Copyright (C) 2020, 2022, 2024  Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * eartrainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EXAMPLE_H
#define EXAMPLE_H

#include <QObject>
#include <QSoundEffect>

#define NOTES_COUNT 88 //number of keys of a classic piano

class NotePlayer: public QObject {
    Q_OBJECT

public:
    NotePlayer();
    ~NotePlayer() = default;
    enum class ChordQualities {Major, Minor, Augmented, Diminished};
    enum class ChordPlayStyle {Harmonic, AscendingArpeggio, DescendingArpeggio};
    enum class IntervalTypes {Harmonic, Ascending, Descending};
    Q_ENUM(ChordQualities)
    Q_ENUM(ChordPlayStyle)
    Q_ENUM(IntervalTypes)
    Q_INVOKABLE void speak();
    Q_INVOKABLE void playNote(int noteID);
    Q_INVOKABLE void playInterval(int firstNoteID, int intervalIndex, IntervalTypes intervalType);
    Q_INVOKABLE void playIntervalAscending(int firstNoteID, int intervalIndex);
    Q_INVOKABLE void playIntervalDescending(int firstNoteID, int intervalIndex);
    Q_INVOKABLE void playIntervalHarmonic(int firstNoteID, int intervalIndex);
    Q_INVOKABLE void playChord(int firstNoteID, ChordQualities chordQuality, ChordPlayStyle chordPlayStyle);
    Q_INVOKABLE int getNotesCount();
    
public slots:
	void nextNote();
    void playNextNoteInSequence();
    void soundPlayingChanged();
private:
    void playChordHarmonic(int firstNoteID, ChordQualities chordQuality);
    void playChordAscendingArpeggio(int firstNoteID, ChordQualities chordQuality);
    void playChordDescendingArpeggio(int firstNoteID, ChordQualities chordQuality);
    int getNthNoteOfChord(int firstNoteID, ChordQualities chordQuality, int n);
    int nextNoteIndex;
	int notesToPlay[4];
	int numberOfNotesToPlay;
    QSoundEffect noteSounds[NOTES_COUNT];
    int major_chord_structure [3] = {0,4,7};
	int minor_chord_structure [3] = {0,3,7};
	int diminished_chord_structure [3] = {0,3,6};
	int augmented_chord_structure [3] = {0,4,8};
};

#endif
